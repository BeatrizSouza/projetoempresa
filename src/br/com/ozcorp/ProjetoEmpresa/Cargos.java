/**
 * @author Beatriz Souza
 */
package br.com.ozcorp.ProjetoEmpresa;

public class Cargos {
	  
	//Atributos
	  private String titulo;
	  private double salarioBase;
	  private double nivelDeAcesso;
	  private Departamento departamento;

	  //Construtor 
	  public Cargos(String titulo, double salarioBase, double nivelDeAcesso, Departamento departamento) {
		super();
		this.titulo = titulo;
		this.salarioBase = salarioBase;
		this.nivelDeAcesso = nivelDeAcesso;
		this.departamento = departamento;
	
	}
    
	 //Getters and Setters 
	public Departamento getDepartamento() {
		return departamento;
	}


	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}


	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public double getNivelDeAcesso() {
		return nivelDeAcesso;
	}

	public void setNivelDeAcesso(double nivelDeAcesso) {
		this.nivelDeAcesso = nivelDeAcesso;
	}
	
	  
     
	  
}
