/**
 * @author Beatriz Souza
 */

package br.com.ozcorp.ProjetoEmpresa;

public class EmpresaTest {
	public static void main(String[] args) {

		Funcionarios Jo�o = new Funcionarios("Jo�o", "1234567", "2345678", "1234", 
				"JoaoLima@gmail.com", "123", "A+", Sexo.MASCULINO, new Cargos("Gerente", 9000, 2,
						new Departamento("Departamento Administrativo", "D.A")));

		// Dados do funcionario
		System.out.println(" Informa��es do usuario ");
		System.out.println("Nome: " + Jo�o.getNome());
		System.out.println("Rg: " + Jo�o.getRg());
		System.out.println("CPF: " + Jo�o.getCpf());
		System.out.println("Matricula: " + Jo�o.getMatricula());
		System.out.println("Email: " + Jo�o.getEmail());
		System.out.println("Senha: " + Jo�o.getSenha());
		System.out.println("Tipo Sanguineo: " + Jo�o.getTipoSanguineo());
		System.out.println("Sexo: " + Jo�o.getSexo().s);
		System.out.println("Titulo: " + Jo�o.getCargo().getTitulo());
		System.out.println("Nivel de acesso: " + Jo�o.getCargo().getNivelDeAcesso());
		System.out.println("Departamento: " + Jo�o.getCargo().getDepartamento().getNomeDoDepartamento());
		System.out.println("Sigla: " + Jo�o.getCargo().getDepartamento().getsigla());

		System.out.println("****************************************************************");

		Funcionarios Marina = new Funcionarios("Marina", "34567890", "369523168", "4321",
				"MarinaSantos@gmail.com", "Azulado1234", "O-", Sexo.FEMININO,
				new Cargos("Diretora ", 30_000_00, 4, 
						new Departamento("Departamento Financeiro", "D.F")));

		// Dados do funcionario
		System.out.println(" Informa��es do usuario ");
		System.out.println("Nome: " + Marina.getNome());
		System.out.println("Rg: " + Marina.getRg());
		System.out.println("CPF: " + Marina.getCpf());
		System.out.println("Matricula: " + Marina.getMatricula());
		System.out.println("Email: " + Marina.getEmail());
		System.out.println("Senha: " + Marina.getSenha());
		System.out.println("Tipo Sanguineo: " + Marina.getTipoSanguineo());
		System.out.println("Sexo: " + Marina.getSexo().s);
		System.out.println("Titulo: " + Marina.getCargo().getTitulo());
		System.out.println("Nivel de acesso: " + Marina.getCargo().getNivelDeAcesso());
		System.out.println("Departamento: " + Marina.getCargo().getDepartamento().getNomeDoDepartamento());
		System.out.println("Sigla: " + Marina.getCargo().getDepartamento().getsigla());

		System.out.println("*******************************************************************");

		Funcionarios Mateus = new Funcionarios("Mateus", "87654932", "1234958", "8954",
				"MateusSouza@gmail.com","TREW254", "O+", Sexo.MASCULINO,
				new Cargos("Analista", 4000_00, 3, 
						new Departamento("Departamento Financeiro", "D.F")));

		//Dados do Funcionario
		System.out.println(" Informa��es do usuario ");
		System.out.println("Nome: " + Mateus.getNome());
		System.out.println("Rg: " + Mateus.getRg());
		System.out.println("CPF: " + Mateus.getCpf());
		System.out.println("Matricula: " + Mateus.getMatricula());
		System.out.println("Email: " + Mateus.getEmail());
		System.out.println("Senha: " + Mateus.getSenha());
		System.out.println("Tipo Sanguineo: " + Mateus.getTipoSanguineo());
		System.out.println("Sexo: " + Mateus.getSexo().s);
		System.out.println("Titulo: " + Mateus.getCargo().getTitulo());
		System.out.println("Nivel de acesso: " + Mateus.getCargo().getNivelDeAcesso());
		System.out.println("Departamento: " + Mateus.getCargo().getDepartamento().getNomeDoDepartamento());
		System.out.println("Sigla: " + Mateus.getCargo().getDepartamento().getsigla());

		System.out.println("****************************************************************************");

		Funcionarios Carla = new Funcionarios("Carla", "2583696", "78541257", "9632",
				"CarlaTorres@gmail.com", "WERS234", "A-", Sexo.FEMININO,
				new Cargos("Secretaria", 2000_00, 0, 
						new Departamento("Recursos Humanos", "R.H")));

		//Dados do Funcionario
		System.out.println(" Informa��es do usuario ");
		System.out.println("Nome: " + Carla.getNome());
		System.out.println("Rg: " + Carla.getRg());
		System.out.println("CPF: " + Carla.getCpf());
		System.out.println("Matricula: " + Carla.getMatricula());
		System.out.println("Email: " + Carla.getEmail());
		System.out.println("Senha: " + Carla.getSenha());
		System.out.println("Tipo Sanguineo: " + Carla.getTipoSanguineo());
		System.out.println("Sexo: " + Carla.getSexo().s);
		System.out.println("Titulo: " + Carla.getCargo().getTitulo());
		System.out.println("Nivel de acesso: " + Carla.getCargo().getNivelDeAcesso());
		System.out.println("Departamento: " + Carla.getCargo().getDepartamento().getNomeDoDepartamento());
		System.out.println("Sigla: " + Carla.getCargo().getDepartamento().getsigla());

		System.out.println("*************************************************************************");

		Funcionarios Henrique = new Funcionarios("Henrique", "7895236", "159357", "7412",
				"HenriqueMachado@gmail.com", "MASTER888", "B-", Sexo.OUTROS,
				new Cargos("Engenheiro", 8500_00, 2, 
						new Departamento("Recursos Humanos", "R.H")));

		//Dados do Funcionario
		System.out.println(" Informa��es do usuario ");
		System.out.println("Nome: " + Henrique.getNome());
		System.out.println("Rg: " + Henrique.getRg());
		System.out.println("CPF: " + Henrique.getCpf());
		System.out.println("Matricula: " + Henrique.getMatricula());
		System.out.println("Email: " + Henrique.getEmail());
		System.out.println("Senha: " + Henrique.getSenha());
		System.out.println("Tipo Sanguineo: " + Henrique.getTipoSanguineo());
		System.out.println("Sexo: " + Henrique.getSexo().s);
		System.out.println("Titulo: " + Henrique.getCargo().getTitulo());
		System.out.println("Nivel de acesso: " + Henrique.getCargo().getNivelDeAcesso());
		System.out.println("Departamento: " + Henrique.getCargo().getDepartamento().getNomeDoDepartamento());
		System.out.println("Sigla: " + Henrique.getCargo().getDepartamento().getsigla());
	}

}
