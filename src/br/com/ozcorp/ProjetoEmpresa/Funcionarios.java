/**
 * @author Beatriz Souza
 */

package br.com.ozcorp.ProjetoEmpresa;

public class Funcionarios {
	
	//Atributos
	
	private String nome;
	private String Rg;
	private String cpf;
 	private String matricula;
	private String Email;
	private String senha;
	private String TipoSanguineo;
	private Sexo sexo;
	private Cargos cargo;
	
	//Contrutor
	
	public Funcionarios(String nome, String rg, String cpf, String matricula, String email,String senha,
			String tipoSanguineo, Sexo sexo, Cargos cargo) {
		super();
		this.nome = nome;
		this.Rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.Email = email;
		this.senha = senha;
		this.TipoSanguineo = tipoSanguineo;
		this.sexo = sexo;
		this.cargo = cargo;
	}

	//Getters & Setters 
	
	public Cargos getCargo() {
		return cargo;
	}

	public void setCargo(Cargos cargo) {
		this.cargo = cargo;
	}
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return Rg;
	}

	public void setRg(String rg) {
		this.Rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		this.Email = email;
	}


	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getTipoSanguineo() {
		return TipoSanguineo;
	}

	public void setTipoSanguineo(String tipoSanguineo) {
		this.TipoSanguineo = tipoSanguineo;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	
	
	

}



