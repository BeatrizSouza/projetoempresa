/**
 * @author Beatriz Souza
 */

package br.com.ozcorp.ProjetoEmpresa;

public class Departamento {
	//Atributos
	private String nomeDoDepartamento;
	private String sigla;

	

    //Construtor
	public Departamento(String nomeDoDepartamento, String sigla) {
		super();
		this.nomeDoDepartamento = nomeDoDepartamento;
		this.sigla = sigla;
	}

    //Getters and Setters
	public String getNomeDoDepartamento() {
		return nomeDoDepartamento;
	}


	public void setNome(String nomeDoDepartamento) {
		this.nomeDoDepartamento = nomeDoDepartamento;
	}


	public String getsigla() {
		return sigla;
	}


	public void setsigla(String sigla) {
		this.sigla = sigla;
	}
	
	
	
	

	
	

}
